<?php 
include('header.php');

?>
<!DOCTYPE html>
<html lang="en" dir="">

<body>
<!-- -----------preload srart------------------ -->
<div class="loadscreen" id="preloader" style="display: none;">

    <div class="loader spinner-bubble spinner-bubble-primary">



    </div>
</div>

<!-- =========================header nav bar=================================   -->

<div>
  
    <div class="main-header">
        <div class="logo">
            <img src="./assets/images/logo.png" alt="">
        </div>

        <!-- <div class="menu-toggle">
            <div></div>
            <div></div>
            <div></div>
        </div> -->

        <div class="d-flex align-items-center">
            <!-- Mega menu -->
            <div class="dropdown mega-menu d-none d-md-block">
                <a href="index.html" class="btn text-muted  mr-3">Home</a>
                <a href="#" class="btn text-muted  mr-3">Feeds</a>
                <a href="#" class="btn text-muted  mr-3" style="background: #639; color: #fff !important;">Leads</a>
                <a href="#" class="btn text-muted  mr-3">Contacts</a>
                <a href="account-lists.php" class="btn text-muted  mr-3">Account</a>

                <a href="#" class="btn text-muted  mr-3" id="dropdownMegaMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h" style="font-size: 20px;"></i></a>

                <div class="dropdown-menu text-left" aria-labelledby="dropdownMenuButton">
                    <div class="row m-0">
                        <!-- <div class="col-md-4 p-4 bg-img">
                            <h2 class="title">Mega Menu <br> Sidebar</h2>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Asperiores natus laboriosam fugit, consequatur.
                            </p>
                            <p class="mb-4">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Exercitationem odio amet eos dolore suscipit placeat.</p>
                            <button class="btn btn-lg btn-rounded btn-outline-warning">Learn More</button>
                        </div> -->
                        
                        <!-- <div class="col-md-4 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">Features</p>
                            <div class="menu-icon-grid w-auto p-0">
                                <a href="#"><i class="i-Shop-4"></i> Home</a>
                                <a href="#"><i class="i-Library"></i> UI Kits</a>
                                <a href="#"><i class="i-Drop"></i> Apps</a>
                                <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                                <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                                <a href="#"><i class="i-Ambulance"></i> Support</a>
                            </div>
                        </div> -->

                        <div class="col-md-12 p-4">
                            <!-- <p class="text-primary text--cap border-bottom-primary d-inline-block">Components</p> -->
                            <ul class="links">
                                <li><a href="#">Opportunity</a></li>
                                <li><a href="#">Activities</a></li>
                                <li><a href="#">Reports</a></li>
                                <li><a href="#">Documents</a></li>
                                <li><a href="#">Campaigns</a></li>
                                <li><a href="#">Visits</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- / Mega menu -->
            <div class="search-bar">
                <input type="text" placeholder="Search">
                <i class="search-icon text-muted i-Magnifi-Glass1"></i>
            </div>
        </div>

        <div style="margin: auto"></div>

        <div class="header-part-right">
            <!-- Full screen toggle -->
            <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
            <!-- Grid menu Dropdown -->
            <div class="dropdown">
                <i class="i-Safe-Box text-muted header-icon" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <div class="menu-icon-grid">
                        <a href="#"><i class="i-Shop-4"></i> Home</a>
                        <a href="#"><i class="i-Library"></i> UI Kits</a>
                        <a href="#"><i class="i-Drop"></i> Apps</a>
                        <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                        <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                        <a href="#"><i class="i-Ambulance"></i> Support</a>
                    </div>
                </div>
            </div>
            <!-- Notificaiton -->
            <div class="dropdown">
                <div class="badge-top-container" role="button" id="dropdownNotification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="badge badge-primary">3</span>
                    <i class="i-Bell text-muted header-icon"></i>
                </div>
                <!-- Notification dropdown -->
                <div class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none" aria-labelledby="dropdownNotification" data-perfect-scrollbar data-suppress-scroll-x="true">
                    <div class="dropdown-item d-flex">
                        <div class="notification-icon">
                            <i class="i-Speach-Bubble-6 text-primary mr-1"></i>
                        </div>
                        <div class="notification-details flex-grow-1">
                            <p class="m-0 d-flex align-items-center">
                                <span>New message</span>
                                <span class="badge badge-pill badge-primary ml-1 mr-1">new</span>
                                <span class="flex-grow-1"></span>
                                <span class="text-small text-muted ml-auto">10 sec ago</span>
                            </p>
                            <p class="text-small text-muted m-0">James: Hey! are you busy?</p>
                        </div>
                    </div>
                    <div class="dropdown-item d-flex">
                        <div class="notification-icon">
                            <i class="i-Receipt-3 text-success mr-1"></i>
                        </div>
                        <div class="notification-details flex-grow-1">
                            <p class="m-0 d-flex align-items-center">
                                <span>New order received</span>
                                <span class="badge badge-pill badge-success ml-1 mr-1">new</span>
                                <span class="flex-grow-1"></span>
                                <span class="text-small text-muted ml-auto">2 hours ago</span>
                            </p>
                            <p class="text-small text-muted m-0">1 Headphone, 3 iPhone x</p>
                        </div>
                    </div>
                    <div class="dropdown-item d-flex">
                        <div class="notification-icon">
                            <i class="i-Empty-Box text-danger mr-1"></i>
                        </div>
                        <div class="notification-details flex-grow-1">
                            <p class="m-0 d-flex align-items-center">
                                <span>Product out of stock</span>
                                <span class="badge badge-pill badge-danger ml-1 mr-1">3</span>
                                <span class="flex-grow-1"></span>
                                <span class="text-small text-muted ml-auto">10 hours ago</span>
                            </p>
                            <p class="text-small text-muted m-0">Headphone E67, R98, XL90, Q77</p>
                        </div>
                    </div>
                    <div class="dropdown-item d-flex">
                        <div class="notification-icon">
                            <i class="i-Data-Power text-success mr-1"></i>
                        </div>
                        <div class="notification-details flex-grow-1">
                            <p class="m-0 d-flex align-items-center">
                                <span>Server Up!</span>
                                <span class="badge badge-pill badge-success ml-1 mr-1">3</span>
                                <span class="flex-grow-1"></span>
                                <span class="text-small text-muted ml-auto">14 hours ago</span>
                            </p>
                            <p class="text-small text-muted m-0">Server rebooted successfully</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Notificaiton End -->

            <!-- User avatar dropdown -->
            <div class="dropdown">
                <div class="user col align-self-end">
                    <img src="./assets/images/faces/1.jpg" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <div class="dropdown-header">
                            <i class="i-Lock-User mr-1"></i> Timothy Carlson
                        </div>
                        <a class="dropdown-item">Account settings</a>
                        <a class="dropdown-item">Billing history</a>
                        <a class="dropdown-item" href="signin.html">Sign out</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<!-- =========================header nav bar end=================================   -->
<!-- ============ Body content start ============= -->

<div class="main-content-wrap d-flex flex-column" style="padding: 0;">
    <div class="card o-hidden mb-4">
        <div class="card-header d-flex align-items-center">
            <h3 class="w-50 float-left card-title m-0">New Users in Datatable</h3>
            <div class="dropdown dropleft text-right w-50 float-right">
              
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a href="leads-create.php" type="button" class="btn btn-secondary"><i class="nav-icon i-Add"></i></a>
                    <button type="button" class="btn btn-secondary"  id="dropdownMenuButton_table3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="    border-top-right-radius: 0.25rem;
                    border-bottom-right-radius: 0.25rem;"><i class="nav-icon i-Gear-2"></i></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton_table3">
                        <a class="dropdown-item" href="leads-create.php">Mass Delete</a>
                        <a class="dropdown-item" href="#">Mass Update</a>
                        <a class="dropdown-item" href="#">Mass Email</a>
                        <a class="dropdown-item" href="#">Approve Leads</a>
                        <a class="dropdown-item" href="#">Add to Campaignss</a>
                        <a class="dropdown-item" href="#">Print View</a>

                    </div>
                </div>
               

            </div>


        </div>
        <div class="card-body">

            <div class="table-responsive">

                <table id="user_table" class=" table table-borderless  text-center">
                    <thead>
                        <tr>

                            <th scope="col">
                                
                                
                              <div class="button-group">
                                 <a data-toggle="dropdown"><i class="i-Box-Full" style="font-size: 18px;"></i></a>
                         <ul class="dropdown-menu">
                            <li><a href="#" class="small dropdown-item" data-value="option1" tabIndex="-1">
    
                                <label class="checkbox checkbox-primary">
                                    <input type="checkbox">
                                    <span>Primary</span>
                                    <span class="checkmark"></span>
                                </label>
    
                               </a></li>
                           <li><a href="#" class="small dropdown-item" data-value="option1" tabIndex="-1">
                            <label class="checkbox checkbox-primary">
                                <input type="checkbox">
                                <span>Primary</span>
                                <span class="checkmark"></span>
                            </label>
                           </a></li>
                           <li><a href="#" class="small dropdown-item" data-value="option2" tabIndex="-1">
                            <label class="checkbox checkbox-primary">
                                <input type="checkbox">
                                <span>Primary</span>
                                <span class="checkmark"></span>
                            </label>
                           </a></li>
                           <li><a href="#" class="small dropdown-item" data-value="option3" tabIndex="-1">
                            <label class="checkbox checkbox-primary">
                                <input type="checkbox">
                                <span>Primary</span>
                                <span class="checkmark"></span>
                            </label>
                           </a></li>
                           <li><a href="#" class="small dropdown-item" data-value="option4" tabIndex="-1">
                            <label class="checkbox checkbox-primary">
                                <input type="checkbox">
                                <span>Primary</span>
                                <span class="checkmark"></span>
                            </label></a></li>
                           <li><a href="#" class="small dropdown-item" data-value="option5" tabIndex="-1">
                            <label class="checkbox checkbox-primary">
                                <input type="checkbox">
                                <span>Primary</span>
                                <span class="checkmark"></span>
                            </label>
                           </a></li>
                           <li><a href="#" class="small dropdown-item" data-value="option6" tabIndex="-1">
                            <label class="checkbox checkbox-primary">
                                <input type="checkbox">
                                <span>Primary</span>
                                <span class="checkmark"></span>
                            </label>
                           </a></li>
                         </ul>
                           </div>
                        
                        
                        </th>

                            <th scope="col">Lead Name</th>
                            <th scope="col">Company</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Lead Source</th>
                            <th scope="col">Lead Owner</th>
                            <th scope="col">Title</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>

                            <th scope="col">
                             
                               							
                        
                        </th>

                            <th scope="col"><input type="text" class="form-control" id="firstName1" placeholder="Enter Lead Name"></th>
                            <th scope="col"><input type="text" class="form-control" id="firstName1" placeholder="Enter Company"></th>
                            <th scope="col"><input type="text" class="form-control" id="firstName1" placeholder="Enter Email"></th>
                            <th scope="col"><input type="text" class="form-control" id="firstName1" placeholder="Enter Phone"></th>
                            <th scope="col"><input type="text" class="form-control" id="firstName1" placeholder="Enter Lead Source"></th>
                            <th scope="col"><input type="text" class="form-control" id="firstName1" placeholder="Enter Lead Owner"></th>
                            <th scope="col"><input type="text" class="form-control" id="firstName1" placeholder="Enter Title"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT * FROM Lead_details";
                            $result = $con->query($sql);
                            
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <th scope="row">
                                            <label class="checkbox checkbox-primary">
                                                <input type="checkbox" >
                                                <span class="checkmark"></span>
                                            </label>
                                        </th>
                                        <td><?php echo $row['lead_name'] ?></td>
                                        <td><?php echo $row['company'] ?></td>
                                        <td><?php echo $row['lead_email'] ?></td>
                                        <td><?php echo $row['mobile_no'] ?></td>
                                        <td><?php echo $row['lead_source'] ?></td>
                                        <td><?php echo $row['lead_owner'] ?></td>
                                        <!-- <?php // if($row['title']=="Active"){ ?>
                                        <td><span class="badge badge-success"><?php // echo $row['title'] ?></span></td>
                                        <?php // } elseif($row['title']=="Pending"){ ?>
                                            <td><span class="badge badge-info"><?php // echo $row['title'] ?></span></td>
                                        <?php // } elseif($row['title']=="Not Active"){ ?>  
                                            <td><span class="badge badge-warning"><?php // echo $row['title'] ?></span></td> 
                                        <?php // } ?>    -->  
                                        <td><?php  echo $row['title'] ?></td> 
                                        <td>
                                            <a href="leads-edit.php?id=<?php echo $row['lead_id']; ?>" class="text-success mr-2">
                                                <i class="nav-icon i-Pen-2 font-weight-bold"></i>
                                            </a>
                                            <a href="leads-delete.php?id=<?php echo $row['lead_id']; ?>" class="text-danger mr-2">
                                                <i class="nav-icon i-Close-Window font-weight-bold"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    
                                <?php    
                                }
                            } else {
                                echo "No results Found.";
                            }
                        ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- ============ Body content End ============= -->

       <!-- ============ Search UI Start ============= -->
       <div class="search-ui">
        <div class="search-header">
            <img src="./assets/images/logo.png" alt="" class="logo">
            <button class="search-close btn btn-icon bg-transparent float-right mt-2">
                <i class="i-Close-Window text-22 text-muted"></i>
            </button>
        </div>

        <input type="text" placeholder="Type here" class="search-input" autofocus>

        <div class="search-title">
            <span class="text-muted">Search results</span>
        </div>

        <div class="search-results list-horizontal">
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-1.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-danger">Sale</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-2.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-3.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-4.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- PAGINATION CONTROL -->
        <div class="col-md-12 mt-5 text-center">
            <nav aria-label="Page navigation example">
                <ul class="pagination d-inline-flex">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    
    <!-- ============ Search UI End ============= -->

    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="assets/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="assets/js/vendor/perfect-scrollbar.min.js"></script>

    <script src="assets/js/vendor/pickadate/picker.js"></script>
    <script src="assets/js/vendor/pickadate/picker.date.js"></script>

    <script src="assets/js/es5/script.min.js"></script>
    <script src="assets/js/es5/sidebar.large.script.min.js"></script>

    <script src="assets/js/form.basic.script.js"></script>

    <script src="assets/js/vendor/echarts.min.js "></script>
    <script src="assets/js/vendor/datatables.min.js"></script>
    <script src="assets/js/es5/echart.options.min.js"></script>
    <script src="assets/js/es5/dashboard.v4.script.min.js "></script>

    <script>
        var options = [];

$( '.dropdown-menu a' ).on( 'click', function( event ) {

   var $target = $( event.currentTarget ),
       val = $target.attr( 'data-value' ),
       $inp = $target.find( 'input' ),
       idx;

   if ( ( idx = options.indexOf( val ) ) > -1 ) {
      options.splice( idx, 1 );
      setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
   } else {
      options.push( val );
      setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
   }

   $( event.target ).blur();
      
   console.log( options );
   return false;
});
    </script>
</body>
</html>