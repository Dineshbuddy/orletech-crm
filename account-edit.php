<?php
include('db.php');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Account-Edit</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="assets/styles/css/themes/lite-purple.min.css">
    <link rel="stylesheet" href="assets/styles/vendor/perfect-scrollbar.css">
    <link rel="stylesheet" href="assets/styles/vendor/pickadate/classic.css">
    <link rel="stylesheet" href="assets/styles/vendor/pickadate/classic.date.css">
    <link href="fontawesome-icons/css/all.css" rel="stylesheet">
</head>

<?php 

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // collect value of input field
    $account_id = $_POST['account_id'];
    $account_owner = $_POST['account_owner'];
    $account_name = $_POST['account_name'];
    $phone_num = $_POST['phone_num'];
    $mob_num = $_POST['mob_num'];
    $parent_account = $_POST['parent_account'];
    $website = $_POST['website'];
    $account_type = $_POST['account_type'];
    $industry = $_POST['industry'];
    $employee_count = $_POST['employee_count'];
    $bill_add_street = $_POST['bill_add_street'];
    $bill_add_city = $_POST['bill_add_city'];
    $bill_add_state = $_POST['bill_add_state'];
    $bill_add_zipcode =  $_POST['bill_add_zipcode'];
    $bill_add_country =   $_POST['bill_add_country'];
    $ship_add_street =   $_POST['ship_add_street'];
    $ship_add_city =   $_POST['ship_add_city'];
    $ship_add_state =   $_POST['ship_add_state'];
    $ship_add_zipcode =   $_POST['ship_add_zipcode'];
    $ship_add_country =   $_POST['ship_add_country'];
    $description = $_POST['description'];


    // echo $leadOwner;
    // echo $address;
    // echo $description;

    
    $sqlQuery = "UPDATE `account_details` SET `account_owner` ='".$account_owner. "', `account_name` ='".$account_name. "', `phone_num` ='".$phone_num."', `mob_num` = '".$mob_num." ',    `parent_account` ='".$parent_account. "', `website` ='".$website. "', `account_type` ='".$account_type."', `industry` ='".$industry."', `employee_count` ='".$employee_count."', 
     `bill_add_street` ='".$bill_add_street."',`bill_add_city` ='".$bill_add_city."',`bill_add_state` ='".$bill_add_state."',`bill_add_zipcode` ='".$bill_add_zipcode."',`bill_add_country` ='".$bill_add_country."',
      `ship_add_street` ='".$ship_add_street."',`ship_add_city` ='".$ship_add_city."',`ship_add_state` ='".$ship_add_state."',`ship_add_zipcode` ='".$ship_add_zipcode."',`ship_add_country` ='".$ship_add_country."',
       `description` ='".$description."' WHERE `account_id` =  $account_id " ;
    //echo $sqlQuery ; die;

    if(mysqli_query($con, $sqlQuery)){
        //echo "Records Updated successfully.";
        // header("Location: leads.php?msg=Success"); exit;
        // echo RedirectURL('leads.php?msg=Success');
        echo '<script type="text/javascript">window.location = "account-lists.php?msg=Success";</script>';


    } else {
        header("Location:account-lists.php?msg=Failed");
        echo "ERROR: Could not able to execute $sqlQuery. " . mysqli_error($con);
    }
 
    
} else if(isset($_GET['id'])) {
    $selectedValues = "SELECT * FROM account_details WHERE account_id = ".$_GET['id'];
    //echo $selectedValues;
    $oldData = $con->query($selectedValues);    
    $data = $oldData->fetch_assoc();                      
    


?>

<body>
    <!-- -----------preload srart------------------ -->
    <div class="loadscreen" id="preloader" style="display: none;">

        <div class="loader spinner-bubble spinner-bubble-primary">



        </div>
    </div>

    <!-- =========================header nav bar=================================   -->

    <div>

        <div class="main-header">
            <div class="logo">
                <img src="./assets/images/logo.png" alt="">
            </div>

            <!-- <div class="menu-toggle">
            <div></div>
            <div></div>
            <div></div>
        </div> -->

            <div class="d-flex align-items-center">
                <!-- Mega menu -->
                <div class="dropdown mega-menu d-none d-md-block">
                    <a href="index.html" class="btn text-muted  mr-3">Home</a>
                    <a href="#" class="btn text-muted  mr-3">Feeds</a>
                    <a href="leads.php" class="btn text-muted  mr-3"
                       >Leads</a>
                    <a href="#" class="btn text-muted  mr-3" >Contacts</a>
                    <a href="account-lists.php" class="btn text-muted  mr-3"style="background: #639; color: #fff !important;">Account</a>

                        <a href="#" class="btn text-muted  mr-3" id="dropdownMegaMenuButton" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h"
                                style="font-size: 20px;"></i></a>

                        <div class="dropdown-menu text-left" aria-labelledby="dropdownMenuButton">
                            <div class="row m-0">
                                <!-- <div class="col-md-4 p-4 bg-img">
                            <h2 class="title">Mega Menu <br> Sidebar</h2>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Asperiores natus laboriosam fugit, consequatur.
                            </p>
                            <p class="mb-4">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Exercitationem odio amet eos dolore suscipit placeat.</p>
                            <button class="btn btn-lg btn-rounded btn-outline-warning">Learn More</button>
                        </div> -->

                                <!-- <div class="col-md-4 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">Features</p>
                            <div class="menu-icon-grid w-auto p-0">
                                <a href="#"><i class="i-Shop-4"></i> Home</a>
                                <a href="#"><i class="i-Library"></i> UI Kits</a>
                                <a href="#"><i class="i-Drop"></i> Apps</a>
                                <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                                <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                                <a href="#"><i class="i-Ambulance"></i> Support</a>
                            </div>
                        </div> -->

                                <div class="col-md-12 p-4">
                                    <!-- <p class="text-primary text--cap border-bottom-primary d-inline-block">Components</p> -->
                                    <ul class="links">
                                        <li><a href="#">Opportunity</a></li>
                                        <li><a href="#">Activities</a></li>
                                        <li><a href="#">Reports</a></li>
                                        <li><a href="#">Documents</a></li>
                                        <li><a href="#">Campaigns</a></li>
                                        <li><a href="#">Visits</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>

                </div>
                <!-- / Mega menu -->
                <div class="search-bar">
                    <input type="text" placeholder="Search">
                    <i class="search-icon text-muted i-Magnifi-Glass1"></i>
                </div>
            </div>

            <div style="margin: auto"></div>

            <div class="header-part-right">
                <!-- Full screen toggle -->
                <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
                <!-- Grid menu Dropdown -->
                <div class="dropdown">
                    <i class="i-Safe-Box text-muted header-icon" role="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="menu-icon-grid">
                            <a href="#"><i class="i-Shop-4"></i> Home</a>
                            <a href="#"><i class="i-Library"></i> UI Kits</a>
                            <a href="#"><i class="i-Drop"></i> Apps</a>
                            <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                            <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                            <a href="#"><i class="i-Ambulance"></i> Support</a>
                        </div>
                    </div>
                </div>
                <!-- Notificaiton -->
                <div class="dropdown">
                    <div class="badge-top-container" role="button" id="dropdownNotification" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <span class="badge badge-primary">3</span>
                        <i class="i-Bell text-muted header-icon"></i>
                    </div>
                    <!-- Notification dropdown -->
                    <div class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none"
                        aria-labelledby="dropdownNotification" data-perfect-scrollbar data-suppress-scroll-x="true">
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Speach-Bubble-6 text-primary mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>New message</span>
                                    <span class="badge badge-pill badge-primary ml-1 mr-1">new</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">10 sec ago</span>
                                </p>
                                <p class="text-small text-muted m-0">James: Hey! are you busy?</p>
                            </div>
                        </div>
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Receipt-3 text-success mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>New order received</span>
                                    <span class="badge badge-pill badge-success ml-1 mr-1">new</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">2 hours ago</span>
                                </p>
                                <p class="text-small text-muted m-0">1 Headphone, 3 iPhone x</p>
                            </div>
                        </div>
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Empty-Box text-danger mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>Product out of stock</span>
                                    <span class="badge badge-pill badge-danger ml-1 mr-1">3</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">10 hours ago</span>
                                </p>
                                <p class="text-small text-muted m-0">Headphone E67, R98, XL90, Q77</p>
                            </div>
                        </div>
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Data-Power text-success mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>Server Up!</span>
                                    <span class="badge badge-pill badge-success ml-1 mr-1">3</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">14 hours ago</span>
                                </p>
                                <p class="text-small text-muted m-0">Server rebooted successfully</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Notificaiton End -->

                <!-- User avatar dropdown -->
                <div class="dropdown">
                    <div class="user col align-self-end">
                        <img src="./assets/images/faces/1.jpg" id="userDropdown" alt="" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                            <div class="dropdown-header">
                                <i class="i-Lock-User mr-1"></i> Timothy Carlson
                            </div>
                            <a class="dropdown-item">Account settings</a>
                            <a class="dropdown-item">Billing history</a>
                            <a class="dropdown-item" href="signin.html">Sign out</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <!-- =========================header nav bar end=================================   -->
    <!-- ============ Body content start ============= -->
    <form class="needs-validation" novalidate method="post" name="account_update" action="<?php echo $_SERVER['PHP_SELF'];?>">


        <div class="Create-lead-header">

            <div class="row">
                <div class="col-md-6">
                    <h2>Account Updation</h2>
                </div>
                <div class="col-md-6" style="text-align: right;">
                    <a href="account-lists.php"><button type="button" class="btn btn-outline-secondary m-1">Cancel</button></a>
                    <button type="submit" class="btn btn-outline-success m-1">Update</button>
                </div>
            </div>
        </div>

        <div class="main-content-wrap d-flex flex-column" style="margin-top: 166px; position: inherit;">

            <div class="Create-lead-image">
                <h4>Account Image</h4>
                <div class="lead-img">
                    <!-- <img src="./assets/images/faces/1.jpg" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->
                    <!-- <img id="blah" src="./assets/images/faces/user-thumbnail.png" alt="your image" /> -->

                    <div class="input-group mb-3 mt-3">
                        <div class="custom-file" style="display: block;">
                            <label class="lead-img" for="inputGroupFile01"><img id="blah"
                                    src="./assets/images/faces/user-thumbnail.png" alt="your image" /></label>
                            <input type="file" class="custom-file-input" id="inputGroupFile01"
                                aria-describedby="inputGroupFileAddon01" onchange="readURL(this);"
                                style="display: none;">
                        </div>
                    </div>
                </div>

            </div>
            <div class="Create-lead-information" style="margin-top:2%;">

                <h4>Account Information</h4>



                <div class="row" style="padding: 0 5%;">

                    <div class="col-md-6 form-group mb-3">
                        <label for="accountowner">Account Owner</label>
                        <select name="account_owner" class="form-control">
                        <?php
                            $sql = "SELECT * FROM lead_owner";
                            $result = $con->query($sql);
                            
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    ?>
                                   <option value="<?php echo $row['lead_owner_name'];  ?>" <?php if (strcmp($data['account_owner'], $row['lead_owner_name']) == 0) { echo "selected"; } ?> ><?php echo $row['lead_owner_name'];  ?></option> 
                                    
                                <?php    
                                }
                            } else {
                                echo "No results Found";
                            }
                        ?>
                        </select>
                    </div>

                    <!-- <div class="col-md-6 form-group mb-3">
                        <label for="picker1">Rating</label>
                        <select class="form-control">
                            <option>None </option>
                            <option>Advertisement </option>
                            <option>Cold call </option>
                            <option>Employee Referral </option>
                            <option>External Referral </option>
                            <option>Online Store </option>
                            <option>Public Relations </option>
                            <option>Sales Email Alias </option>
                            <option>Seminar partner</option>
                            <option>Internal seminar </option>
                            <option>Trade show </option>
                            <option>Web download </option>
                            <option>Web Research </option>
                            <option>Chat </option>
                            <option>Twitter </option>
                            <option>Facebook</option>
                            <option>Google+ </option>
                            <option>Linked In</option>  
                        </select>
                    </div> -->



                    <div class="col-md-6 form-group mb-3">
                        <label for="account_name">Account Name</label>
                        <input type="text" class="form-control" name="account_name" id="firstName1" placeholder="Type the Name" value="<?php echo $data['account_name'];?>"  >
                    </div>

                    <div class="col-md-6 form-group mb-3">
                        <label for="phone_num">Phone
                        </label>
                        <input  class="form-control" name="phone_num" id="firstName1" placeholder="Type the Phone Number" required="" value="<?php echo $data['phone_num'];?>" >
                        <div class="invalid-feedback">
                            Please provide a Phone Number.
                        </div>
                    </div>

<!-- 
                    <div class="col-md-6 form-group mb-3">
                        <label for="exampleInputEmail1">Account Site</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" required="">
                        <div class="invalid-feedback">
                            Please provide a Account Site.
                        </div>
                    </div> -->

                    <div class="col-md-6 form-group mb-3">
                        <label for="mob_num">Mobile</label>
                        <input  class="form-control" name="mob_num" id="exampleInputEmail1" placeholder="Type the Mobile Number" required="" value="<?php echo $data['mob_num'];?>" >
                        <div class="invalid-feedback">
                        Please provide a Mobile Number.
                        </div>
                    </div>

                    <div class="col-md-6 form-group mb-3">
                        <label for="parent_account">Parent Account</label>
                        <input type="text" class="form-control" name="parent_account" id="phone" value="<?php echo $data['parent_account'];?>"  placeholder="Type The Parent Account">
                    </div>

                    <div class="col-md-6 form-group mb-3">
                        <label for="website">Website</label>
                        <input type="text" class="form-control" name="website" id="phone" value="<?php echo $data['website'];?>"  placeholder="Type Your Web Address">
                    </div>

                    <!-- <div class="col-md-6 form-group mb-3">
                        <label for="website">Account Number                       </label>
                        <input class="form-control" id="website" placeholder="Web address" >
        
                    </div>
                    <div class="col-md-6 form-group mb-3">
                        <label for="phone">Ticker Symbol</label>
                        <input class="form-control" id="phone" placeholder="">
                    </div> -->
                    <div class="col-md-6 form-group mb-3">
                        <label for="picker1">Account Type</label>
                        <select name="account_type" class="form-control">
                        <?php
                            $sql = "SELECT * FROM account_type";
                            $result = $con->query($sql);
                            
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    ?>
                                   <option value="<?php echo $row['type']; ?>" <?php if (strcmp($data['account_type'], $row['type']) == 0) { echo "selected"; } ?> ><?php echo $row['type'];  ?></option> 
                                    
                                <?php    
                                }
                            } else {
                                echo "No results Found";
                            }
                        ?>
                        </select>
                    </div>
                    <!-- <div class="col-md-6 form-group mb-3">
                        <label for="picker1">Ownership</label>
                        <select class="form-control">
                            <option>None </option>
                            <option>Advertisement </option>
                            <option>Cold call </option>
                            <option>Employee Referral </option>
                            <option>External Referral </option>
                            <option>Online Store </option>
                        </select>
                    </div> -->
                    <div class="col-md-6 form-group mb-3">
                        <label for="picker1">Industry</label>
                        <select name="industry" class="form-control">
                        <?php
                            $sql1 = "SELECT * FROM industry_details";
                            $result1 = $con->query($sql1);                            
                            if ($result1->num_rows > 0) {
                                //echo "Test";
                                // output data of each row
                                while($row1 = $result1->fetch_assoc()) {
                                    ?>
                                   <option value="<?php echo $row1['industry'];  ?>" <?php if (strcmp($data['industry'], $row1['industry']) == 0 ) { echo "selected"; } ?> ><?php echo $row1['industry'];  ?></option> 
                                    
                                <?php    
                                }
                            } 
                        ?>
                        </select>
                    </div>
                    <div class="col-md-6 form-group mb-3">
                        <label for="website">Employees</label>
                        <input name="employee_count" class="form-control" id="website" value="<?php echo $data['employee_count'];?>"  placeholder="No of Employees" >
        
                    </div>
                    <!-- <div class="col-md-6 form-group mb-3">
                        <label for="website">Annual Revenue</label>
                        <input class="form-control" id="website" placeholder="Web address" >
        
                    </div>
                    <div class="col-md-6 form-group mb-3">
                        <label for="phone">SIC Code</label>
                        <input class="form-control" id="phone" placeholder="">
                    </div> -->
 </div>
 </div>
 <div class="Create-lead-information" style="margin-top: 7%;">

    <h4>Address Information</h4>



    <div class="row" style="padding: 0 5%;">
<div class="col-md-6 form-group mb-3">
            <label for="street">Billing Street</label>
            <input name ="bill_add_street" type="text" class="form-control" id="firstName1" value="<?php echo $data['bill_add_street'];?>"  placeholder="" >
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="street">Shipping Street</label>
            <input name ="ship_add_street" type="text" class="form-control" id="firstName1" value="<?php echo $data['ship_add_street'];?>"  placeholder="" >
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="city">Billing City</label>
            <input name ="bill_add_city" type="text" class="form-control" id="firstName1" value="<?php echo $data['bill_add_city'];?>" placeholder="" >
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="ship_add_city">Shipping City</label>
            <input name ="ship_add_city" type="text" class="form-control" id="firstName1" value="<?php echo $data['ship_add_city'];?>" placeholder="" >
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="state">Billing State</label>
            <input name ="bill_add_state" type="text" class="form-control" id="firstName1" value="<?php echo $data['bill_add_state'];?>" placeholder="" >
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="ship_add_state">Shipping State</label>
            <input name="ship_add_state" type="text" class="form-control" id="firstName1" value="<?php echo $data['ship_add_state'];?>"  placeholder="" >
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="zipcode">Billing Code</label>
            <input name="bill_add_zipcode" type="text" class="form-control" id="firstName1" value="<?php echo $data['bill_add_zipcode'];?>" placeholder="" >
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="ship_add_zipcode">Shipping Code</label>
            <input name="ship_add_zipcode" type="text" class="form-control" id="firstName1" value="<?php echo $data['ship_add_zipcode'];?>" placeholder="" >
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="country">Billing Country</label>
            <input name="bill_add_country" type="text" class="form-control" id="firstName1" value="<?php echo $data['bill_add_country'];?>" placeholder="" >
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="ship_add_country">Shipping Country</label>
            <input name="ship_add_country" type="text" class="form-control" id="firstName1" value="<?php echo $data['ship_add_country'];?>" placeholder="" >
        </div>

</div>
</div>
<div class="Create-lead-information" style="margin-top: 7%;">

    <h4> Description Information
    </h4>
<div class="row" style="padding: 0 5%;">

        <div class="col-md-6 form-group mb-3">
            <label for="description">Description</label>
            <div class="input-group" style="width:150%">         
                <textarea name ="description" class="form-control" rows="5"><?php echo $data['description'];?></textarea>
            </div>
        </div>


</div>
</div>
</form>
<!-- ============ Body content End ============= -->
 <!-- ============ Search UI Start ============= -->
    <div class="search-ui">
        <div class="search-header">
            <img src="./assets/images/logo.png" alt="" class="logo">
            <button class="search-close btn btn-icon bg-transparent float-right mt-2">
                <i class="i-Close-Window text-22 text-muted"></i>
            </button>
        </div>

        <input type="text" placeholder="Type here" class="search-input" autofocus>

        <div class="search-title">
            <span class="text-muted">Search results</span>
        </div>

        <div class="search-results list-horizontal">
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-1.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div
                            class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-danger">Sale</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-2.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div
                            class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-3.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div
                            class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-4.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div
                            class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- PAGINATION CONTROL -->
        <div class="col-md-12 mt-5 text-center">
            <nav aria-label="Page navigation example">
                <ul class="pagination d-inline-flex">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <!-- ============ Search UI End ============= -->

    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="assets/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="assets/js/vendor/perfect-scrollbar.min.js"></script>

    <script src="assets/js/vendor/pickadate/picker.js"></script>
    <script src="assets/js/vendor/pickadate/picker.date.js"></script>

    <script src="assets/js/es5/script.min.js"></script>
    <script src="assets/js/es5/sidebar.large.script.min.js"></script>

    <script src="assets/js/form.basic.script.js"></script>

    <script src="assets/js/form.validation.script.js"></script>



    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>


</body>

</html>

<?php } ?>