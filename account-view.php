<!DOCTYPE html>
<html lang="en" dir="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Account View</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="assets/styles/css/themes/lite-purple.min.css">
    <link rel="stylesheet" href="assets/styles/vendor/perfect-scrollbar.css">
    <link rel="stylesheet" href="assets/styles/vendor/pickadate/classic.css">
    <link rel="stylesheet" href="assets/styles/vendor/pickadate/classic.date.css">
    <link href="fontawesome-icons/css/all.css" rel="stylesheet">
</head>

<body>
    <!-- -----------preload srart------------------ -->
    <div class="loadscreen" id="preloader" style="display: none;">

        <div class="loader spinner-bubble spinner-bubble-primary">



        </div>
    </div>

    <!-- =========================header nav bar=================================   -->

    <div>

        <div class="main-header">
            <div class="logo">
                <img src="./assets/images/logo.png" alt="">
            </div>

            <!-- <div class="menu-toggle">
            <div></div>
            <div></div>
            <div></div>
        </div> -->

            <div class="d-flex align-items-center">
                <!-- Mega menu -->
                <div class="dropdown mega-menu d-none d-md-block">
                    <a href="index.html" class="btn text-muted  mr-3">Home</a>
                    <a href="#" class="btn text-muted  mr-3">Feeds</a>
                    <a href="leads.php" class="btn text-muted  mr-3"
                       >Leads</a>
                    <a href="#" class="btn text-muted  mr-3">Contacts</a>
                    <a href="account-lists.php" class="btn text-muted  mr-3" style="background: #639; color: #fff !important;">Account</a>

                        <a href="#" class="btn text-muted  mr-3" id="dropdownMegaMenuButton" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h"
                                style="font-size: 20px;"></i></a>

                        <div class="dropdown-menu text-left" aria-labelledby="dropdownMenuButton">
                            <div class="row m-0">
                                <!-- <div class="col-md-4 p-4 bg-img">
                            <h2 class="title">Mega Menu <br> Sidebar</h2>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Asperiores natus laboriosam fugit, consequatur.
                            </p>
                            <p class="mb-4">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Exercitationem odio amet eos dolore suscipit placeat.</p>
                            <button class="btn btn-lg btn-rounded btn-outline-warning">Learn More</button>
                        </div> -->

                                <!-- <div class="col-md-4 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">Features</p>
                            <div class="menu-icon-grid w-auto p-0">
                                <a href="#"><i class="i-Shop-4"></i> Home</a>
                                <a href="#"><i class="i-Library"></i> UI Kits</a>
                                <a href="#"><i class="i-Drop"></i> Apps</a>
                                <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                                <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                                <a href="#"><i class="i-Ambulance"></i> Support</a>
                            </div>
                        </div> -->

                                <div class="col-md-12 p-4">
                                    <!-- <p class="text-primary text--cap border-bottom-primary d-inline-block">Components</p> -->
                                    <ul class="links">
                                        <li><a href="#">opportunity</a></li>
                                        <li><a href="#">Activities</a></li>
                                        <li><a href="#">Reports</a></li>
                                        <li><a href="#">Documents</a></li>
                                        <li><a href="#">Campaigns</a></li>
                                        <li><a href="#">Visits</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>

                </div>
                <!-- / Mega menu -->
                <div class="search-bar">
                    <input type="text" placeholder="Search">
                    <i class="search-icon text-muted i-Magnifi-Glass1"></i>
                </div>
            </div>

            <div style="margin: auto"></div>

            <div class="header-part-right">
                <!-- Full screen toggle -->
                <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
                <!-- Grid menu Dropdown -->
                <div class="dropdown">
                    <i class="i-Safe-Box text-muted header-icon" role="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="menu-icon-grid">
                            <a href="#"><i class="i-Shop-4"></i> Home</a>
                            <a href="#"><i class="i-Library"></i> UI Kits</a>
                            <a href="#"><i class="i-Drop"></i> Apps</a>
                            <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                            <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                            <a href="#"><i class="i-Ambulance"></i> Support</a>
                        </div>
                    </div>
                </div>
                <!-- Notificaiton -->
                <div class="dropdown">
                    <div class="badge-top-container" role="button" id="dropdownNotification" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <span class="badge badge-primary">3</span>
                        <i class="i-Bell text-muted header-icon"></i>
                    </div>
                    <!-- Notification dropdown -->
                    <div class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none"
                        aria-labelledby="dropdownNotification" data-perfect-scrollbar data-suppress-scroll-x="true">
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Speach-Bubble-6 text-primary mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>New message</span>
                                    <span class="badge badge-pill badge-primary ml-1 mr-1">new</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">10 sec ago</span>
                                </p>
                                <p class="text-small text-muted m-0">James: Hey! are you busy?</p>
                            </div>
                        </div>
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Receipt-3 text-success mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>New order received</span>
                                    <span class="badge badge-pill badge-success ml-1 mr-1">new</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">2 hours ago</span>
                                </p>
                                <p class="text-small text-muted m-0">1 Headphone, 3 iPhone x</p>
                            </div>
                        </div>
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Empty-Box text-danger mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>Product out of stock</span>
                                    <span class="badge badge-pill badge-danger ml-1 mr-1">3</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">10 hours ago</span>
                                </p>
                                <p class="text-small text-muted m-0">Headphone E67, R98, XL90, Q77</p>
                            </div>
                        </div>
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Data-Power text-success mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">
                                <p class="m-0 d-flex align-items-center">
                                    <span>Server Up!</span>
                                    <span class="badge badge-pill badge-success ml-1 mr-1">3</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">14 hours ago</span>
                                </p>
                                <p class="text-small text-muted m-0">Server rebooted successfully</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Notificaiton End -->

                <!-- User avatar dropdown -->
                <div class="dropdown">
                    <div class="user col align-self-end">
                        <img src="./assets/images/faces/1.jpg" id="userDropdown" alt="" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                            <div class="dropdown-header">
                                <i class="i-Lock-User mr-1"></i> Timothy Carlson
                            </div>
                            <a class="dropdown-item">Account settings</a>
                            <a class="dropdown-item">Billing history</a>
                            <a class="dropdown-item" href="signin.html">Sign out</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <!-- =========================header nav bar end=================================   -->
    <!-- ============ Body content start ============= -->
   


        <div class="Create-lead-header">

            <div class="row">
                <div class="col-md-6">
                    <h2>Contact View</h2>
                </div>
                <div class="col-md-6" style="text-align: right;">
                    <button type="button" class="btn btn-outline-success m-1">Send Email</button>
                  <a href="#"> <button type="submit" class="btn btn-outline-success m-1">Convert</button> </a>
                  <a href="contact-create.html"><button type="button" class="btn btn-outline-secondary m-1">Edit</button></a>
                  <a href="#" class="btn text-muted  mr-3" id="dropdownMegaMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h" style="font-size: 20px;"></i></a>
                  <div class="dropdown-menu text-left" aria-labelledby="dropdownMenuButton">
                    <div class="row m-0">

                        <div class="col-md-12 p-4">
                         
                            <ul class="links">
                                <li><a href="#">Clone</a></li>
                                <li><a href="#">Delete</a></li>
                                <li><a href="#"> Print Preview</a></li>
                                <li><a href="#">Find and Merge Duplicates</a></li>
                                <li><a href="#"> Mail Merge</a></li>
                                <li><a href="#"> Customize Business Card</a></li>
                                <li><a href="#"> Organize Lead Details</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>

        <div class="main-content-wrap d-flex flex-column" style="margin-top: 166px; position: inherit;">

            <div class="Create-lead-image" >
                <a href="leads.html"><i class="i-Back1" style="font-size: 30px;"></i></a>
                
                <div class="lead-img">
                    <!-- <img src="./assets/images/faces/1.jpg" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->
                    <!-- <img id="blah" src="./assets/images/faces/user-thumbnail.png" alt="your image" /> -->

                    <div class="input-group mb-3 mt-3">
                        <div class="custom-file" style="display: block;">
                            <label class="lead-img" for="inputGroupFile01"><img id="blah"
                                    src="./assets/images/faces/user-thumbnail.png" alt="your image" /></label>
                            <input type="file" class="custom-file-input" id="inputGroupFile01"
                                aria-describedby="inputGroupFileAddon01" onchange="readURL(this);"
                                style="display: none;">

                                <div class="sectionTitle">
                                    <span class="lead-first-name">Grandbiz Trading Enterprises   </span>
                                    <span> &nbsp;-&nbsp;</span>
                                   
                                </div>

                                <table>
                                    <tr>
                                        <td class="lable" >
                                           Account Owner
                                        </td>
                                        <td class="lable-value" >
                                            Anandhi
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="lable" >
                                            Industry 
                                        </td>
                                        <td class="lable-value" >
                                            IT
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="lable" >
                                            Employees
                                        </td>
                                        <td class="lable-value" >
                                            -
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="lable" >
                                           Annual Revenue
                                        </td>
                                        <td class="lable-value" >
                                           -
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="lable" >
                                           Phone
                                        </td>
                                        <td class="lable-value" >
                                         -
                                        </td>
                                    </tr>
                                </table>

                        </div>
                    </div>
                </div>

            </div>

<div class="box"style="height: 160px;background-color: #f8f8f8;">

<div class="row">
    <div class="col-6">
        <h4 style="padding: 26px 0px 0px 115px;color: #332e38;font-weight: 700;">Next Action</h4>
        <p style="padding: 10px 0px 0px 115px;font-size: 16px;">Email</p>
        <p style="padding: 8px 0px 0px 115px;font-size: 16px;">Product Demo</p>
    </div>
    <div class="col-6 left"style="border-left: solid 1px #dbdbdb;border-left-height: 12px;padding: 0px 0px 0px 61px;top: 26px;height: 112px;">
        <h4 style="padding: 0px 0px 0px 0px;color: #332e38;font-weight: 700;">Contact</h4>
        <div class="right"style="padding: 10px 0px 0px 0px;">
            <img src="assets\images\faces\user-thumbnail.png"style="border-radius: 50px;width: 30px;position: absolute;">
            <b><p style="padding: 2px 0px 0px 50px;font-size: 16px;">Mr.Arun P</p></b>
            <a href="#"><p style="padding: 0px 0px 0px 50px;font-size: 16px;color: #0f7fe1;">example@gamil.com</p></a>
        </div>
        

    </div>
</div>


</div>
            <div class="Create-lead-information" style="margin-top: 2%;">

                <h4>Account Information</h4>
               

                <div class="row" style="padding: 0 5%;">
                    <div class="col-md-6">

                        <table>
                            <tr>
                                <td class="lable" >
                                    Account Owner
                                </td>
                                <td class="lable-value" >
                                    Anandhi
                                </td>
                            </tr>
        
                            <tr>
                                <td class="lable" >
                                    Account Name
                                </td>
                                <td class="lable-value" >
                                    Example Title
                                </td>
                            </tr>
                            <tr>
                                <td class="lable" >
                                   Account Site
                                </td>
                                <td class="lable-value" >
                                  -
                                </td>
                            </tr>
                            <tr>
                                <td class="lable" >
                                    Parent Account
                                </td>
                                <td class="lable-value" >
                                    -
                                </td>
                            </tr>
        
                            <tr>
                                <td class="lable" >
                                    Account Number
                                </td>
                                <td class="lable-value" >
                                   -
                                </td>
                            </tr>
        
                            <tr>
                                <td class="lable" >
                                    Account Type
                                </td>
                                <td class="lable-value" >
                                    -
                                </td>
                            </tr>
        
                            <tr>
                                <td class="lable" >
                                    Industry
                                </td>
                                <td class="lable-value" >
                                    -
                                </td>
                            </tr>
        
                            <tr>
                                <td class="lable" >
                                    Annual Revenue
                                </td>
                                <td class="lable-value" >
                                   <span>-</span>
                                </td>
                            </tr>
        
                            <tr>
                                <td class="lable" >
                                    Created By
                                </td>
                                <td class="lable-value" >
                                    ramlv.orchid ramlv.orchid
                                </td>
                            </tr>
        
                            <tr>
                                <td class="lable" >
                                    Rating
                                </td>
                                <td class="lable-value" >
                                    Anandhi
                                </td>
                            </tr>
                            
                        </table>

                    </div>
                    <div class="col-md-6">

                        <table>
                        <tr>
                            <td class="lable" >
                                Phone
                            </td>
                            <td class="lable-value" >
                                Orletech
                            </td>
                        </tr>

                        <tr>
                            <td class="lable" >
                                Fax
                            </td>
                            <td class="lable-value" >
                               <span>Ms.</span>Anandhi
                            </td>
                        </tr>

                    

                        <tr>
                            <td class="lable" >
                                Website
                            </td>
                            <td class="lable-value" >
                                8854467
                            </td>
                        </tr>

                        <tr>
                            <td class="lable" >
                                Ticker Symbol
                            </td>
                            <td class="lable-value" >
                                www.example.com
                            </td>
                        </tr>

                        <tr>
                            <td class="lable" >
                                Ownership
                            </td>
                            <td class="lable-value" >
                                Attempted to Contact
                            </td>
                        </tr>


                        <tr>
                            <td class="lable" >
                                Employees
                            </td>
                            <td class="lable-value" >
                                Acquired
                            </td>
                        </tr>

                        <tr>
                            <td class="lable" >
                                SIC Code
                            </td>
                            <td class="lable-value" >
                                Anandhi
                            </td>
                        </tr>

                        <tr>
                            <td class="lable" >
                                Modified By
                            </td>
                            <td class="lable-value" >
                                _
                            </td>
                        </tr>

                        
                    </table>

                    </div>
                </div>

            </div>


            <div class="Create-lead-Address" style="margin-top: 3%;">

                <h4>Address Information</h4>


                <div class="row" style="padding: 0 7%;">
                    <div class="col-md-6">

                        <table>
                            <tr>
                                <td class="lable" >
                                    Street
                                </td>
                                <td class="lable-value" >
                                    bharathinagar
                                </td>
                            </tr>
        
                            <tr>
                                <td class="lable" >
                                    State
                                </td>
                                <td class="lable-value" >
                                    pondicherry
                                </td>
                            </tr>
        
                            <tr>
                                <td class="lable" >
                                    Country
                                </td>
                                <td class="lable-value" >
                                    india
                                </td>
                            </tr>
                            
                        </table>

                    </div>
                    <div class="col-md-6">

                        <table>
                        <tr>
                            <td class="lable" >
                                City
                            </td>
                            <td class="lable-value" >
                                puducherry
                            </td>
                        </tr>

                        <tr>
                            <td class="lable" >
                                Zip Code
                            </td>
                            <td class="lable-value" >
                                605008
                            </td>
                        </tr>
                        
                    </table>

                    </div>
                </div>




            </div>

            <div class="Create-lead-Address" style="margin-top: 3%;">

                <h4> Description Information</h4>

                <div class="row" style="padding: 0 8%;">
                    <table>
                        <tr>
                            <td class="lable" >
                                Description
                            </td>
                            <td class="lable-value" >
                                hello
                            </td>
                        </tr>
                        </table>
                </div>
            </div>

           

            <div class="Create-lead-note" style="margin-top: 3%">

                <h4>Notes</h4>

                <div class="row " style="padding: 0 8%;">
                <div class="col-md-8 form-group mb-3 Create-lead-note-textbox">
                    <!-- <label for="exampleFormControlTextarea1">Description</label> -->
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Add Note"></textarea>
                    <div class="row">
                        <div class="col-md-6" style="padding: 10px 15px;">
                            <!-- <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile02">
                                    <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Choose
                                                file</label>
                                </div>
                                
                            </div> -->
                        </div>
                        <div class="col-md-6" style="text-align: right; padding: 10px 10px;">
                            <button type="submit" class="btn btn-outline-success m-1">Save</button>
                            <button type="button" class="btn btn-outline-secondary m-1">Cancel</button>
                        </div>
                    </div>
                   
                  </div>
                  
                </div>
            </div>


            <div class="Create-lead-Attachments" style="margin-top: 3%">

                <div class="row">
                    <div class="col-md-6">
                        <h4>Attachments</h4>
                    </div>
                    <div class="col-md-5" style="text-align: right;">

                        <a href="#" class="btn text-muted  mr-3" id="dropdownMegaMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="file-attach">Attach ▼</span></a>

                        <div class="dropdown-menu text-left" aria-labelledby="dropdownMenuButton">
                            <div class="row m-0">

                                <div class="col-md-12 p-4">
                                    <!-- <p class="text-primary text--cap border-bottom-primary d-inline-block">Components</p> -->
                                    <ul class="links">
                                        <li><a href="#">Upload File</a></li>
                                        <li><a href="#">Link (URL)</a></li>
                                        <li><a href="#">Reports</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
               
                <div class="lead-Attachments-file">
                   <p style="margin: 0;">No Attachment</p>
                   
                </div>

            </div>

            <div class="Create-lead-Attachments" style="margin-top: 3%;">

                <div class="row">
                    <div class="col-md-6">
                        <h4>Open Activities</h4>
                    </div>
                </div>
                
               
                <div class="lead-Attachments-file">
                    <p style="margin: 0;">No records found</p>
                    <a href="#" class="create-link"> <i class="i-Add"></i> Task </a>
                   <a href="#" class="create-link"> <i class="i-Add"></i> Event </a>
                   <a href="#" class="create-link"> <i class="i-Add"></i> Call </a>
                </div>



            </div>

            <div class="Create-lead-Attachments" style="margin-top: 3%;">

                <div class="row">
                    <div class="col-md-6">
                        <h4>Contacts</h4>
                        
                    </div>
                </div>
                
               
                <div class="lead-Attachments-file">
                    <a href="#" class="create-link"> <i class="i-Add"></i> Add Contact</a>
                </div>

            </div>
            <div class="Create-lead-Attachments" style="margin-top: 3%;">

                <div class="row">
                    <div class="col-md-6">
                        <h4>Emails</h4>
                    </div>
                </div>
                
               
                <div class="lead-Attachments-file">
                    <p style="margin: 0;">No records found</p>
                </div>

            </div>

            <div class="Create-lead-Attachments" style="margin-top: 3%;">

                <div class="row">
                    <div class="col-md-6">
                        <h4>Invited Events</h4>
                    </div>
                </div>
                
               
                <div class="lead-Attachments-file">
                    <p style="margin: 0;">No records found</p>
                </div>

            </div>

            <div class="Create-lead-Attachments" style="margin: 3% 0 5% 0;">

                <div class="row">
                    <div class="col-md-6">
                        <h4>Campaigns</h4>
                    </div>
                </div>
                
               
                <div class="lead-Attachments-file">
                    <p style="margin: 0;">No records found</p>
                </div>

            </div>

        </div>
    

   







    <!-- ============ Body content End ============= -->















    <!-- ============ Search UI Start ============= -->
    <div class="search-ui">
        <div class="search-header">
            <img src="./assets/images/logo.png" alt="" class="logo">
            <button class="search-close btn btn-icon bg-transparent float-right mt-2">
                <i class="i-Close-Window text-22 text-muted"></i>
            </button>
        </div>

        <input type="text" placeholder="Type here" class="search-input" autofocus>

        <div class="search-title">
            <span class="text-muted">Search results</span>
        </div>

        <div class="search-results list-horizontal">
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-1.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div
                            class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-danger">Sale</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-2.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div
                            class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-3.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div
                            class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <!-- TUMBNAIL -->
                        <img src="./assets/images/products/headphone-4.jpg" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div
                            class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <!-- OTHER DATA -->
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- PAGINATION CONTROL -->
        <div class="col-md-12 mt-5 text-center">
            <nav aria-label="Page navigation example">
                <ul class="pagination d-inline-flex">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <!-- ============ Search UI End ============= -->

    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="assets/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="assets/js/vendor/perfect-scrollbar.min.js"></script>

    <script src="assets/js/vendor/pickadate/picker.js"></script>
    <script src="assets/js/vendor/pickadate/picker.date.js"></script>

    <script src="assets/js/es5/script.min.js"></script>
    <script src="assets/js/es5/sidebar.large.script.min.js"></script>

    <script src="assets/js/form.basic.script.js"></script>

    <script src="assets/js/form.validation.script.js"></script>



    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>


</body>

</html>