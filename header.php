<?php
include('db.php');
?>
<!DOCTYPE html>
<html lang="en" dir="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Orletech CRM</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="assets/styles/css/themes/lite-purple.min.css">
    <link rel="stylesheet" href="assets/styles/vendor/perfect-scrollbar.css">
    <link rel="stylesheet" href="assets/styles/vendor/pickadate/classic.css">
    <link rel="stylesheet" href="assets/styles/vendor/pickadate/classic.date.css">
    <link href="fontawesome-icons/css/all.css" rel="stylesheet">
</head>
<body>
<!-- -----------preload srart------------------ -->
<div class="loadscreen" id="preloader" style="display: none;">
    <div class="loader spinner-bubble spinner-bubble-primary">
    </div>
</div>

<!-- =========================header nav bar=================================   -->
<div>
    <div class="main-header">
        <div class="logo">
            <img src="./assets/images/logo.png" alt="userImage">
        </div>

        <!-- <div class="menu-toggle">
            <div></div>
            <div></div>
            <div></div>
        </div> -->

        <div class="d-flex align-items-center">
            <!-- Mega menu -->
            <div class="dropdown mega-menu d-none d-md-block">
                <a href="header.php" class="btn text-muted  mr-3" style="background: #639; color: #fff !important;">Home</a>
                <a href="leads.php" class="btn text-muted  mr-3">Leads</a>
                <a href="contact.php" class="btn text-muted  mr-3">Contacts</a>
                <a href="account.php" class="btn text-muted  mr-3">Account</a>
                <a href="oppourtunity.php" class="btn text-muted  mr-3">Opportunity</a>

                <a href="#" class="btn text-muted  mr-3" id="dropdownMegaMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h" style="font-size: 20px;"></i></a>

                <div class="dropdown-menu text-left" aria-labelledby="dropdownMenuButton">
                    <div class="row m-0">
                        <!-- <div class="col-md-4 p-4 bg-img">
                            <h2 class="title">Mega Menu <br> Sidebar</h2>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Asperiores natus laboriosam fugit, consequatur.
                            </p>
                            <p class="mb-4">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Exercitationem odio amet eos dolore suscipit placeat.</p>
                            <button class="btn btn-lg btn-rounded btn-outline-warning">Learn More</button>
                        </div> -->
                        
                        <!-- <div class="col-md-4 p-4">
                            <p class="text-primary text--cap border-bottom-primary d-inline-block">Features</p>
                            <div class="menu-icon-grid w-auto p-0">
                                <a href="#"><i class="i-Shop-4"></i> Home</a>
                                <a href="#"><i class="i-Library"></i> UI Kits</a>
                                <a href="#"><i class="i-Drop"></i> Apps</a>
                                <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                                <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                                <a href="#"><i class="i-Ambulance"></i> Support</a>
                            </div>
                        </div> -->

                        <div class="col-md-12 p-4">
                            <!-- <p class="text-primary text--cap border-bottom-primary d-inline-block">Components</p> -->
                            <ul class="links">
                                <li><a href="opportunity-view.html">Campaigns</a></li>
                                <li><a href="#">Activities</a></li>
                                <li><a href="#">Reports</a></li>
                                <li><a href="#">Documents</a></li>
                                <li><a href="campaigns-lists.html">Campaigns</a></li>
                                <li><a href="#">Visits</a></li>
                                
                                
                            </ul>
                        </div>
                    </div>
                </div>  

            </div>
            <!-- / Mega menu -->
            <div class="search-bar">
                <input type="text" placeholder="Search">
                <i class="search-icon text-muted i-Magnifi-Glass1"></i>
            </div>
        </div>

        <div style="margin: auto"></div>

        <div class="header-part-right">
            <!-- Full screen toggle -->
            <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
            <!-- Grid menu Dropdown -->
            <a href="settings.html">
                <i class="i-Gear-2 text-muted header-icon" role="button"></i>
                <!-- <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <div class="menu-icon-grid">
                        <a href="#"><i class="i-Shop-4"></i> Home</a>
                        <a href="#"><i class="i-Library"></i> UI Kits</a>
                        <a href="#"><i class="i-Drop"></i> Apps</a>
                        <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                        <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                        <a href="#"><i class="i-Ambulance"></i> Support</a>
                    </div>
                </div> -->
            </a>
            <!-- Notificaiton -->

            <!-- Notificaiton End -->

            <!-- User avatar dropdown -->
            <div class="dropdown">
                <div class="user col align-self-end">
                    <img src="./assets/images/faces/1.jpg" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <div class="dropdown-header">
                            <i class="i-Lock-User mr-1"></i> Timothy Carlson
                        </div>
                        <a class="dropdown-item">Account settings</a>
                        <a class="dropdown-item">Billing history</a>
                        <a class="dropdown-item" href="signin.html">Sign out</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<!-- =========================header nav bar end=================================   -->



    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="assets/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="assets/js/vendor/perfect-scrollbar.min.js"></script>

    <script src="assets/js/vendor/pickadate/picker.js"></script>
    <script src="assets/js/vendor/pickadate/picker.date.js"></script>

    <script src="assets/js/es5/script.min.js"></script>
    <script src="assets/js/es5/sidebar.large.script.min.js"></script>

    <script src="assets/js/form.basic.script.js"></script>
</body>
</html>
